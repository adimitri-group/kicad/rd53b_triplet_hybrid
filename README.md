Schematics for ITkPixV1 triplet hybrid:
- no data sharing
- no CMD forwarding
- up to 4 links per FE
- Voffset sharing
- 51 pin connector (data, cmd, LPenable, Vsense, MUX, NTC, HV)
- LV: solder pads

v1.0 (2020-07-24):
- Initial version of the schematics

v1.1 (2020-07-27):
- LPenable AC coupled to each chip separately

v1.2 (2020-08-15):
- Voffset corrected
- Added a footprint with RD53B pads
- Added a footprint for the data connector

v1.3 (2020-08-25):
- Vina-GND and Vind-GND capacitors replaced with one Vin-GND capacitor
- Voffset corrected
- Renaming of differential pair nets so that they end with "P" and "N"

v1.4 (2020-11-12):
- Added BoM
- HV filter change
- Updated values for SLDO external resistors
- Added one GND on the data connector (instead of GND_C)
- Separation of nets: GND_REFA
